# Song Credit

No Sense Running (Lost City) - [Jake Kaufman](https://virt.bandcamp.com/album/strike-the-earth-shovel-knight-arranged), CC BY-NC-SA 3.0

The Science Wizard (Explodatorium) - [Jake Kaufman](https://virt.bandcamp.com/album/strike-the-earth-shovel-knight-arranged), CC BY-NC-SA 3.0

Cash out - [Jordan Winslow](https://jordanwinslow.me/RoyaltyFreeMusic/), Attribution

Epiphany - [Jordan Winslow](https://jordanwinslow.me/RoyaltyFreeMusic/), Attribution

River - [Jordan Winslow](https://jordanwinslow.me/RoyaltyFreeMusic/), Attribution

Lovely ⑨ - [Frozen Starfall](https://frozenstarfall.bandcamp.com/album/sommertrip), CC BY-NC 3.0

Sommertrip (crouka Remix feat. Selphius) - [Frozen Starfall](https://frozenstarfall.bandcamp.com/album/sommertrip), CC BY-NC 3.0
