extends Spatial

const DB_OFFSET = 9
const DB_INCREASE_SPEED = 8
const DB_DECAY_SPEED = 6

var music_bus

var current_left_db
var current_right_db

var volume_bar_materials = {}

var venue_emissive_material

func _ready():
	music_bus = AudioServer.get_bus_index("Music")
	current_left_db = 0
	current_right_db = 0

	volume_bar_materials.c = $OuterCase/VolumeBarC.get_surface_material(0)
	
	volume_bar_materials.l1 = $OuterCase/VolumeBarL1.get_surface_material(0)
	volume_bar_materials.l2 = $OuterCase/VolumeBarL2.get_surface_material(0)
	volume_bar_materials.l3 = $OuterCase/VolumeBarL3.get_surface_material(0)
	volume_bar_materials.l4 = $OuterCase/VolumeBarL4.get_surface_material(0)
	volume_bar_materials.l5 = $OuterCase/VolumeBarL5.get_surface_material(0)
	volume_bar_materials.l6 = $OuterCase/VolumeBarL6.get_surface_material(0)
	volume_bar_materials.l7 = $OuterCase/VolumeBarL7.get_surface_material(0)
	volume_bar_materials.l8 = $OuterCase/VolumeBarL8.get_surface_material(0)
	volume_bar_materials.l9 = $OuterCase/VolumeBarL9.get_surface_material(0)
	
	volume_bar_materials.r1 = $OuterCase/VolumeBarR1.get_surface_material(0)
	volume_bar_materials.r2 = $OuterCase/VolumeBarR2.get_surface_material(0)
	volume_bar_materials.r3 = $OuterCase/VolumeBarR3.get_surface_material(0)
	volume_bar_materials.r4 = $OuterCase/VolumeBarR4.get_surface_material(0)
	volume_bar_materials.r5 = $OuterCase/VolumeBarR5.get_surface_material(0)
	volume_bar_materials.r6 = $OuterCase/VolumeBarR6.get_surface_material(0)
	volume_bar_materials.r7 = $OuterCase/VolumeBarR7.get_surface_material(0)
	volume_bar_materials.r8 = $OuterCase/VolumeBarR8.get_surface_material(0)
	volume_bar_materials.r9 = $OuterCase/VolumeBarR9.get_surface_material(0)
	
	$OuterRim/ControlPanel/Screen.material_override.albedo_texture = $Viewport.get_texture()
	
	venue_emissive_material = $OuterRim.get_surface_material(1)
	
func _process(delta):
	if Global.root_parent.get_node("SongPlayer").is_playing():
		if venue_emissive_material.emission_energy < 1:
			venue_emissive_material.emission_energy += (delta / 4)
		else:
			venue_emissive_material.emission_energy = 1
	else:
		if venue_emissive_material.emission_energy > 0.2:
			venue_emissive_material.emission_energy -= (delta / 4)
		else:
			venue_emissive_material.emission_energy = 0.2
	
		
	if current_left_db < AudioServer.get_bus_peak_volume_left_db(music_bus, 0) * 2 + DB_OFFSET:
		current_left_db = AudioServer.get_bus_peak_volume_left_db(music_bus, 0) * 2 + DB_OFFSET
	else:
		current_left_db -= delta * DB_DECAY_SPEED
	
	if current_right_db < AudioServer.get_bus_peak_volume_right_db(music_bus, 0) * 2 + DB_OFFSET:
		current_right_db = AudioServer.get_bus_peak_volume_right_db(music_bus, 0) * 2 + DB_OFFSET
	else:
		current_right_db -= delta * DB_DECAY_SPEED
		
	#current_left_db = AudioServer.get_bus_peak_volume_left_db(music_bus, 0) * 2 + DB_OFFSET
	#current_right_db = AudioServer.get_bus_peak_volume_right_db(music_bus, 0) * 2 + DB_OFFSET

	for vbm in volume_bar_materials.values():
		if vbm.emission_energy > 0:
			vbm.emission_energy -= delta

	if current_left_db > 2:
		volume_bar_materials.c.emission_energy = 1
	if current_left_db > 5:
		volume_bar_materials.l1.emission_energy = 1
	if current_left_db > 6:
		volume_bar_materials.l2.emission_energy = 1
	if current_left_db > 7:
		volume_bar_materials.l3.emission_energy = 1
	if current_left_db > 8.5:
		volume_bar_materials.l4.emission_energy = 1
	if current_left_db > 9:
		volume_bar_materials.l5.emission_energy = 1
	if current_left_db > 9.5:
		volume_bar_materials.l6.emission_energy = 1
	if current_left_db > 10:
		volume_bar_materials.l7.emission_energy = 1
	if current_left_db > 10.5:
		volume_bar_materials.l8.emission_energy = 1
	if current_left_db > 11:
		volume_bar_materials.l9.emission_energy = 1
		
	if current_right_db > 2:
		volume_bar_materials.c.emission_energy = 1
	if current_right_db > 5:
		volume_bar_materials.r1.emission_energy = 1
	if current_right_db > 6:
		volume_bar_materials.r2.emission_energy = 1
	if current_right_db > 7:
		volume_bar_materials.r3.emission_energy = 1
	if current_right_db > 8.5:
		volume_bar_materials.r4.emission_energy = 1
	if current_right_db > 9:
		volume_bar_materials.r5.emission_energy = 1
	if current_right_db > 9.5:
		volume_bar_materials.r6.emission_energy = 1
	if current_right_db > 10:
		volume_bar_materials.r7.emission_energy = 1
	if current_right_db > 10.5:
		volume_bar_materials.r8.emission_energy = 1
	if current_right_db > 11:
		volume_bar_materials.r9.emission_energy = 1

func pop_dartboard(board_index):
	match board_index:
		0:
			$OuterRim/Dartboard0.pop()
		1:
			$OuterRim/Dartboard1.pop()
		2:
			$OuterRim/Dartboard2.pop()
		3:
			$OuterRim/Dartboard3.pop()
		4:
			$OuterRim/Dartboard4.pop()

func set_control_mode():
	$PlayModeAnimationPlayer.play("control_mode")
	# $Viewport/ControlUI.set_control_mode()
	get_tree().call_group("Dart", "kill")
	
	# save score here
	_save()
	
func set_play_mode():
	$PlayModeAnimationPlayer.play("play_mode")
	$Viewport/ControlUI.set_play_mode()

func _save():
	if get_parent().score > Songs.song_max_score[Songs.song[$Viewport/ControlUI.current_song]]:
		Songs.song_max_score[Songs.song[$Viewport/ControlUI.current_song]] = get_parent().score
	if get_parent().max_combo > Songs.song_max_combo[Songs.song[$Viewport/ControlUI.current_song]]:
		Songs.song_max_combo[Songs.song[$Viewport/ControlUI.current_song]] = get_parent().max_combo
	Songs.save()
