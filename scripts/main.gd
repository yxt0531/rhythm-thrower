extends Spatial

var score
var combo
var max_combo

var venue_light_color
var venue_light_material

func _ready():
	score = 0
	combo = 0
	max_combo = 0
	
	venue_light_color = $Venue/OuterCase.get_surface_material(1).albedo_color
	venue_light_material = $Venue/OuterCase.get_surface_material(1)
	
	Global.root_parent = self
	
func play_song(song_index):
	if not $SongPlayer.is_playing():
		$SongPlayer.play(Songs.song[song_index])
		$Venue.set_play_mode()
		reset()

func stop():
	if $SongPlayer.is_playing():
		$SongPlayer.stop()
		$Venue.set_control_mode()

func add_score(added_score):
	score += int(added_score)

func increment_combo():
	combo += 1
	if combo > max_combo:
		max_combo = combo

func reset_combo():
	combo = 0

func reset():
	score = 0
	combo = 0
	max_combo = 0

func _on_SongPlayer_animation_finished(anim_name):
	$Venue.set_control_mode()
