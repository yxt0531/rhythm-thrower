extends Spatial

func _ready():
	hide()

func _on_Area_body_entered(body):
	if visible:
		Global.root_parent.get_node("Player").translation = Vector3(0, 0, 0)
		hide()
