extends Control

func _process(delta):
	if Global.root_parent.get_node("Player").arvr_interface != null or OS.get_name() == "Android":
		queue_free()

func _on_ButtonPrevSong_button_down():
	if not Global.root_parent.get_node("SongPlayer").is_playing():
		Global.root_parent.get_node("Venue/Viewport/ControlUI").prev_song()

func _on_ButtonNextSong_button_down():
	if not Global.root_parent.get_node("SongPlayer").is_playing():
		Global.root_parent.get_node("Venue/Viewport/ControlUI").next_song()

func _on_ButtonPlay_button_down():
	Global.root_parent.play_song(Global.root_parent.get_node("Venue/Viewport/ControlUI").current_song)

func _on_ButtonStop_button_down():
	Global.root_parent.stop()


func _on_ButtonHit2_button_down():
	Global.root_parent.get_node("Venue/OuterRim/Dartboard2")._on_AreaMediumScore_body_entered(null)

func _on_ButtonHit0_button_down():
	Global.root_parent.get_node("Venue/OuterRim/Dartboard0")._on_AreaMediumScore_body_entered(null)

func _on_ButtonHit1_button_down():
	Global.root_parent.get_node("Venue/OuterRim/Dartboard1")._on_AreaMediumScore_body_entered(null)

func _on_ButtonHit3_button_down():
	Global.root_parent.get_node("Venue/OuterRim/Dartboard3")._on_AreaMediumScore_body_entered(null)

func _on_ButtonHit4_button_down():
	Global.root_parent.get_node("Venue/OuterRim/Dartboard4")._on_AreaMediumScore_body_entered(null)
