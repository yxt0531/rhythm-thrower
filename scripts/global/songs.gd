extends Node

var song = ["the_science_wizard", "sommertrip_crouka_remix", "epiphany", "no_sense_running", "cash_out", "lovely_9", "river"]

var song_name = {
	"no_sense_running": "No Sense Running",
	"the_science_wizard": "The Science Wizard",
	"cash_out": "Cash Out",
	"epiphany": "Epiphany",
	"river": "River",
	"lovely_9": "Lovely 9",
	"sommertrip_crouka_remix": "Sommertrip (Remix)"
}

var song_max_score = { }

var song_max_combo = { }

# for saving
const SCORE_SAVE_PATH = "user://songs.score"
const COMBO_SAVE_PATH = "user://songs.combo"

var save_file

func _ready():
	save_file = File.new()
	_load()
	
func save():
	save_file.open(SCORE_SAVE_PATH, File.WRITE)
	save_file.store_string(to_json(song_max_score))
	save_file.close()
	
	save_file.open(COMBO_SAVE_PATH, File.WRITE)
	save_file.store_string(to_json(song_max_combo))
	save_file.close()

func _load():
	if save_file.file_exists(SCORE_SAVE_PATH):
		save_file.open(SCORE_SAVE_PATH, File.READ)
		song_max_score = parse_json(save_file.get_line())
		save_file.close()
	
	if save_file.file_exists(COMBO_SAVE_PATH):
		save_file.open(COMBO_SAVE_PATH, File.READ)
		song_max_combo = parse_json(save_file.get_line())
		save_file.close()
