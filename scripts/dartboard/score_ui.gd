extends Control

var screen

func _ready():
	screen = get_parent().get_parent().get_node("Screen")
	
func _process(delta):
	if screen.material_override.albedo_color.a > 0:
		screen.material_override.albedo_color.a -= delta
	else:
		screen.material_override.albedo_color.a = 0

func display_add_score(score):
	$AddedScoreLabel.text = "+" + str(int(score))
	screen.material_override.albedo_color.a = 1
