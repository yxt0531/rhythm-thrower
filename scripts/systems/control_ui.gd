extends Control

var current_song

func _ready():
	$DebugBackground.visible = false
	current_song = 0
	
	_update_song_name()

func _process(delta):
	$ScoreMaxCombo.text = "Score: " + str(Global.root_parent.score) + "\nMax Combo: " + str(Global.root_parent.max_combo)
	
func next_song():
	if current_song < Songs.song.size() - 1:
		current_song += 1
	else:
		current_song = 0
		
	_update_song_name()
		
func prev_song():
	if current_song == 0:
		current_song = Songs.song.size() - 1
	else:
		current_song -= 1
		
	_update_song_name()

func _update_song_name():
	set_control_mode()
	$LabelSongName.text = Songs.song_name[Songs.song[current_song]]

func set_control_mode():
	$LabelSongName.show()
	$Play.show()
	$ScoreMaxCombo.hide()
	$ScoreMaxComboHistoric.hide()
	
func set_play_mode():
	update_max_score_combo()
	$LabelSongName.hide()
	$Play.hide()
	$ScoreMaxCombo.show()
	$ScoreMaxComboHistoric.show()
	
func update_max_score_combo():
	if Songs.song_max_score.has(Songs.song[current_song]) and Songs.song_max_combo.has(Songs.song[current_song]):
		$ScoreMaxComboHistoric.text = "Highest Score: " + str(Songs.song_max_score[Songs.song[current_song]]) + "\nHighest Combo: " + str(Songs.song_max_combo[Songs.song[current_song]])
	else:
		Songs.song_max_score[Songs.song[current_song]] = 0
		Songs.song_max_combo[Songs.song[current_song]] = 0
		$ScoreMaxComboHistoric.text = "Highest Score: " + str(Songs.song_max_score[Songs.song[current_song]]) + "\nHighest Combo: " + str(Songs.song_max_combo[Songs.song[current_song]])
