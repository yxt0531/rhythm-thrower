extends RigidBody

const LOOK_AT_DELAY = 0.2
const LIFETIME = 5
const LINEAR_SPEED_CLAMP = 100

var look_at_timer
var life_timer

var throwed

func _ready():
	look_at_timer = 0
	life_timer = 0

	throwed = false
	
func _physics_process(delta):
	if throwed:
			
		if linear_velocity.length() >= LINEAR_SPEED_CLAMP:
			queue_free()
			
		if look_at_timer >= LOOK_AT_DELAY:
			look_at(translation + linear_velocity, Vector3.UP)
			look_at_timer = 0
		else:
			look_at_timer += delta
	
		if life_timer >= LIFETIME:
			queue_free()
		elif not $Dart.visible and not $CPUParticles0.emitting:
			queue_free()
		elif linear_velocity.length() < 1:
			queue_free()
		else:
			life_timer += delta
	

func throw(velocity):
	linear_velocity = velocity
	look_at(translation + linear_velocity, Vector3.UP)
	gravity_scale = 0.2
	$CPUParticles.emitting = true
	
	throwed = true
	
	#$ImpactPositionFix.get_collision_point()

func explode():
	# linear_velocity = Vector3(0, 0, 0)
	$Dart.visible = false
	$CPUParticles.visible = false
	
	$CPUParticles0.emitting = true
	$CPUParticles1.emitting = true
	$CPUParticles2.emitting = true
	$CPUParticles3.emitting = true

func kill():
	if throwed:
		queue_free()
