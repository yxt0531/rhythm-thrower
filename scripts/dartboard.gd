extends Spatial

const HIT_ROTATION_SPEED = 10
const REST_DELAY = 1.5

const LOW_SCORE = 20
const MEDIUM_SCORE = 25
const HIGH_SCORE = 30

export var color = 0

var popped_up
var hit
var rest_timer

var colors = []

var score_multiplier

func _ready():
	popped_up = false
	hit = false
	rest_timer = 0
	
	score_multiplier = 5.0
	
	colors.append(Color("ffffff"))
	colors.append(Color("67f4ff"))
	colors.append(Color("ff8967"))
	colors.append(Color("f473b5"))
	colors.append(Color("cd67ff"))
	colors.append(Color("e8f473"))
	
	_apply_color()
	
	$Screen.material_override.albedo_texture = $Viewport.get_texture()
	
func _physics_process(delta):
	if popped_up and not hit:
		rest_timer += delta
		
		if rest_timer >= REST_DELAY:
			# rest
			rest_timer = 0
			$AnimationPlayer.play("rest")
			
		if score_multiplier > 1:
			score_multiplier -= delta * 3
		else:
			score_multiplier = 1
			
	elif hit and popped_up:
		$AnimationPlayer.stop()
		$Body.rotate_z(delta * HIT_ROTATION_SPEED)
		
		if $Body.rotation_degrees.z > 0:
			$Body.rotation_degrees.z = 0
			popped_up = false
			hit = false
		
			# $AnimationPlayer.play("pop_up")

func _process(delta):
	# rotate face
	if $Body/Face.rotation_degrees.y < 360:
		$Body/Face.rotate_y(-delta)
	else:
		$Body/Face.rotation_degrees.y = 0
	
#	if Input.is_action_just_pressed("ui_select"):
#		pop()
		
	if $AnimationPlayer.is_playing() and $AnimationPlayer.current_animation == "halo_shrink":
		if $Halo.get_surface_material(0).albedo_color.a < 1:
			$Halo.get_surface_material(0).albedo_color.a += delta
		else:
			$Halo.get_surface_material(0).albedo_color.a = 1
	else:
		if $Halo.get_surface_material(0).albedo_color.a > 0:
			$Halo.get_surface_material(0).albedo_color.a -= delta * 10
		else:
			$Halo.get_surface_material(0).albedo_color.a = 0
			

func _on_AnimationPlayer_animation_finished(anim_name):
	match anim_name:
		"halo_shrink":
			_pop()
		"rest":
			popped_up = false
			Global.root_parent.reset_combo()

func pop():
	if popped_up == false:
		_play_halo()

func _play_halo():
	$Halo.get_surface_material(0).albedo_color.a = 0
	$AnimationPlayer.play("halo_shrink")

func _pop():
	hit = false
	popped_up = true
	
	rest_timer = 0
	score_multiplier = 5
	$AnimationPlayer.play("pop_up")

func _apply_color():
	match color:
		0:
			$Body/Face.set_surface_material(0, load("res://assets/dartboard/dartboard_face_white.material"))
		1:
			$Body/Face.set_surface_material(0, load("res://assets/dartboard/dartboard_face_cyan.material"))
		2:
			$Body/Face.set_surface_material(0, load("res://assets/dartboard/dartboard_face_orange.material"))
		3:
			$Body/Face.set_surface_material(0, load("res://assets/dartboard/dartboard_face_pink.material"))
		4:
			$Body/Face.set_surface_material(0, load("res://assets/dartboard/dartboard_face_purple.material"))
		5:
			$Body/Face.set_surface_material(0, load("res://assets/dartboard/dartboard_face_yellow.material"))
	$Halo.get_surface_material(0).albedo_color = colors[color]
	$Halo.get_surface_material(0).emission = colors[color]
	$Body.get_surface_material(1).albedo_color = colors[color]
	$Body.get_surface_material(1).emission = colors[color]

func _on_AreaLowScore_body_entered(body):
	if not hit and popped_up:
		hit = true
		Global.root_parent.add_score(LOW_SCORE * score_multiplier)
		Global.root_parent.increment_combo()
		$Viewport/ScoreUI.display_add_score(LOW_SCORE * score_multiplier)
	if body != null:
		body.explode()
		
func _on_AreaMediumScore_body_entered(body):
	if not hit and popped_up:
		hit = true
		Global.root_parent.add_score(MEDIUM_SCORE * score_multiplier)
		Global.root_parent.increment_combo()
		$Viewport/ScoreUI.display_add_score(MEDIUM_SCORE * score_multiplier)
	if body != null:
		body.explode()


func _on_AreaHighScore_body_entered(body):
	if not hit and popped_up:
		hit = true
		Global.root_parent.add_score(HIGH_SCORE * score_multiplier)
		Global.root_parent.increment_combo()
		$Viewport/ScoreUI.display_add_score(HIGH_SCORE * score_multiplier)
	if body != null:
		body.explode()
