extends Spatial

var control_ui

var easter_egg_trigger_timer

func _ready():
	control_ui = get_parent().get_parent().get_node("Viewport/ControlUI")
	
	easter_egg_trigger_timer = 0
	
func _process(delta):
	if easter_egg_trigger_timer > 4:
		# trigger easter egg
		Global.root_parent.get_node("Room1").show()
		Global.root_parent.get_node("Player").translation = Global.root_parent.get_node("Room1").translation
		
		easter_egg_trigger_timer = 0
	elif easter_egg_trigger_timer > 0:
		easter_egg_trigger_timer -= delta

func _on_ButtonRightArea_body_entered(body):
	if not Global.root_parent.get_node("SongPlayer").is_playing():
		body.explode()
		control_ui.next_song()

func _on_ButtonLeftArea_body_entered(body):
	if not Global.root_parent.get_node("SongPlayer").is_playing():
		body.explode()
		control_ui.prev_song()

func _on_ButtonQuitArea_body_entered(body):
	if not Global.root_parent.get_node("SongPlayer").is_playing():
		get_tree().quit()
	else:
		easter_egg_trigger_timer += 1

func _on_ButtonStopArea_body_entered(body):
	if Global.root_parent.get_node("SongPlayer").is_playing():
		body.explode()
		Global.root_parent.stop()

func _on_ScreenAreaTrigger_body_entered(body):
	if not Global.root_parent.get_node("SongPlayer").is_playing():
		body.explode()
		Global.root_parent.play_song(control_ui.current_song)

# debug start
#func _process(delta):
#	if Input.is_action_just_pressed("ui_select"):
#		Global.root_parent.play_song(control_ui.current_song)
#
#	if Input.is_action_just_pressed("ui_cancel"):
#		Global.root_parent.stop()
#
#	if Input.is_action_just_pressed("ui_right"):
#		if not Global.root_parent.get_node("SongPlayer").is_playing():
#			control_ui.next_song()
#
#	if Input.is_action_just_pressed("ui_left"):
#		if not Global.root_parent.get_node("SongPlayer").is_playing():
#			control_ui.prev_song()
# debug end
